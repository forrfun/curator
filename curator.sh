#!/bin/bash
CURRENT_WORKSPACE_PATH=`pwd`
HOME_PATH=`test -h "$0" && dirname $(readlink "$0") || dirname "$0"`

CRAWLER="scrapy crawl"
MAKER="python maker.py"
DB="python urldataDB.py"
DATAFILE="urldata"

init ()
{
	# check homepage_list file is existed, or not.
	if [ ! -e homepage_list ]
	then
		echo "	homepage_list file is needed. please make homepage_list file"
		exit 1
	fi
}

delete_old_data()
{
	while read line
	do
		if [ ${line:0:1} == '#' ]
		then
			echo "	$line is commented."
			continue
		fi

		# delete old data file
		if [ -e $line ]
		then
			echo "	old data($line) was already existed."
			echo "	rm $line"
			rm $line
		fi
	done < homepage_list
}

crawl_page()
{
	while read line
	do
		if [ ${line:0:1} == '#' ]
		then
			echo "	$line is commented."
			continue
		fi

		echo "	crawl $line page"
		$CRAWLER $line 2>&1
	done < homepage_list
}

make_urldata()
{
	cd maker
	if [ -e $DATAFILE ]; then
		rm $DATAFILE
	fi

	while read line
	do
		if [ ${line:0:1} == '#' ]
		then
			echo "	$line is commented."
			continue
		fi

		echo "	make urldata through $line"
		$MAKER $line
	done < ../homepage_list

	echo "	save data to DB"
	$DB $DATAFILE

	cd -
}

cd $HOME_PATH
echo "Step 1. initial"
init
echo "Step 2. remove old data"
delete_old_data
echo "Step 3. crawl item data"
crawl_page
echo "Step 4. make urldata and save urldata to DB"
make_urldata
cd $CURRENT_WORKSPACE_PATH
echo "Crawling item data was finished."
