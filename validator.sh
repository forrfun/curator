#!/bin/bash
CURRENT_WORKSPACE_PATH=`pwd`
HOME_PATH=`test -h "$0" && dirname $(readlink "$0") || dirname "$0"`

VALIDATOR="python validator.py"

validate()
{
	cd maker
	$VALIDATOR
}

cd $HOME_PATH
validate
cd $CURRENT_WORKSPACE_PATH
