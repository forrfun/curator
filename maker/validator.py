import sys
import urllib2
import datetime
from mariaDB import mariaDBops

def data_validation(url):
    req = urllib2.Request('http://www.'+url)

    try:
        resp = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        if e.code == 404:
            print "HttpError response %d" % (e.code)
            return False
    except urllib2.URLError as e:
        # Not an HTTP-specific error (e.g. connection refused)
        print "URLError response %d" % (e.code)

    return True

def block_this_item(item):
    print "%s item is blocked" % (item)
    DB = mariaDBops()
    DB.block_item(item)

def unblock_this_item(item):
    print "%s item is unblocked" % (item)
    DB = mariaDBops()
    DB.unblock_item(item)

def main():
    DB = mariaDBops()
    for item in DB.get_items('100000'):
        valid = 0
        # item[0] is id
        # item[1] is image URL
        # item[2] is link URL
        # item[3] is price
        # item[4] is block status 
        if data_validation(item[1]):
            valid += 1

        if data_validation(item[2]):
            valid += 1

        # if both image URL and link URL are valid, let it decide a valid case.
        print "check %s id" % (item[0])
        if valid == 2 and item[4] == '2':
            unblock_this_item(item[1])
        # invalid(404) case
        elif item[4] == '0':
            block_this_item(item[1])
        else:
            print "id %s item doesn't have to need a status change." % (item[0])

# start python script
start = datetime.datetime.now()
startDateTime = start.strftime('%Y-%m-%d %H:%M:%S')
print startDateTime
main()
end = datetime.datetime.now()
endDateTime = end.strftime('%Y-%m-%d %H:%M:%S')
print endDateTime
