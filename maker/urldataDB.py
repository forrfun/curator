import sys
from mariaDB import mariaDBops

if len(sys.argv) is 1:
	print "need arg (read data file name)"
	exit(0);

urldata = sys.argv[1]
datafile = open(urldata, 'r')

img_url = 0
link_url = 1
price = 2
state = 3
item_id = 4

DB = mariaDBops()

for line in datafile:
	item = line.split()
	DB.add_item(item[link_url], item[img_url], item[price])
