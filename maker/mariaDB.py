#!/usr/bin/python

import MySQLdb

from MySQLdb.constants import ER

class mariaDBops:
    DB_ip = "127.0.0.1"
    DB_id = "bikini"
    DB_pwd = "qlzlslapp!"
    DB = "bikinidb"
    #DB = "bikiniDBtest"
    table = "bikini_django_app_bikinidata"
    STATUS_UNBLOCK = '0'
    STATUS_BLOCK = '2'
    
    insert_sql = "INSERT INTO "+DB+"."+table+" (imgurl, linkurl, price, utime) VALUES (%s,%s,%s, NOW())"
    update_sql = "UPDATE "+DB+"."+table+" SET imgurl=%s, linkurl=%s, price=%s, utime=NOW() WHERE imgurl = %s"
    select_sql = "SELECT id, imgurl, linkurl, price, block FROM "+DB+"."+table+" WHERE id <= %s"
    reset_id_sql = "ALTER TABLE "+DB+"."+table+" AUTO_INCREMENT=1"
    #can't use LIMIT?
    #http://mysql-python.sourceforge.net/MySQLdb.html 
    block_sql = "UPDATE "+DB+"."+table+" SET block="+STATUS_BLOCK+" WHERE imgurl = %s"
    unblock_sql = "UPDATE "+DB+"."+table+" SET block="+STATUS_UNBLOCK+" WHERE imgurl = %s"
    update_id_sql = "UPDATE "+DB+"."+table+" SET id=%s WHERE id=%s"

    def __init__(self):
        self.db = MySQLdb.connect(self.DB_ip, self.DB_id, self.DB_pwd, self.DB)
        self.cursor = self.db.cursor()
        self.cursor.execute("SELECT VERSION()")
        data = self.cursor.fetchone()
        print "Database version : %s " % data

    def __del__(self):
        self.cursor.close()
        self.db.close()

    def add_item(self, link_url, img_url, price):
        try:
            print "try insert: "+ img_url
            self.cursor.execute(self.insert_sql, (img_url, link_url, price))
        except MySQLdb.Error as e:
            try:
                if e.args[0] is not ER.DUP_ENTRY:
                    print "duplicated entry try update: "+str(e)
                    self.cursor.execute(self.reset_id_sql)
                    self.cursor.execute(self.update_sql, (img_url, link_url, price, img_url))
            except Exception as e:
                print "Exception : "+str(e)
                self.db.rollback()
        except Exception as e:
            print "System Exception : "+str(e)
            self.db.rollback()
        self.db.commit()

    def get_items(self, limit):
        print "try get items("+limit+")"
        self.cursor.execute(self.select_sql, [limit])
        return self.cursor.fetchall()

    def block_item(self, img_url):
        print "try block items("+img_url+")"
        try:
            self.cursor.execute(self.block_sql, [img_url])
        except Exception as e:
            print "Exception : "+str(e)
            self.db.rollback()
        self.db.commit()

    def unblock_item(self, img_url):
        print "try unblock items("+img_url+")"
        try:
            self.cursor.execute(self.unblock_sql, [img_url])
        except Exception as e:
            print "Exception : "+str(e)
            self.db.rollback()
        self.db.commit()

    def update_id(self, old_id, new_id):
        print "try update items("+str(old_id)+" to "+str(new_id)+")"
        try:
            self.cursor.execute(self.update_id_sql, (new_id, old_id))
        except Exception as e:
            print "Exception : "+str(e)
            self.db.rollback()
        self.db.commit()

    def swap_id(self, id_1, id_2):
        tmp_id = '999999'
        self.update_id(id_1, tmp_id)
        self.update_id(id_2, id_1)
        self.update_id(tmp_id, id_2)

