from mariaDB import mariaDBops
import random

DB = mariaDBops()

for target_id in range(512, 1000):
    DB.swap_id(target_id, random.randrange(512,1112))
