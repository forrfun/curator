import sys
from mariaDB import mariaDBops

if len(sys.argv) is 2:
	print "need arg (swap target id 1, 2)"
	exit(0);

DB = mariaDBops()

DB.swap_id(sys.argv[1], sys.argv[2])
