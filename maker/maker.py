import sys
# coding=utf-8
def parseint(string):
    return int(''.join([x for x in string if x.isdigit()]))

def file_len(f):
	ret = 0
	for l in f:
		ret = ret + 1
	return ret

if len(sys.argv) is 1:
	print "need arg"
	exit(0);

sitename = sys.argv[1]
inputfile = open('../' + sitename, 'r')
outputfile = open(sitename + '.html', 'w')
datafile = open('urldata', 'a+')

item = []
full_items = []

for line in inputfile :
	if line.rfind('jpg') != -1 or line.rfind('gif') != -1:
		if item == [] :
			item.append(line)
		elif len(item) == 2 :
			print "warning : old data format file (has not price)"
			full_items.append(item[0])
			full_items.append(item[1])
			full_items.append("0\n")
			item = []
			item.append(line)
		else :
			print "error : data parsing"
	else :
		item.append(line)
		if len(item) == 3 :
			if item[0] not in full_items :
				full_items.append(item[0]) #image url
				full_items.append(item[1]) #link url
				full_items.append(item[2]) #price
			item = []

if len(full_items)%3 != 0 :
	print 'fail to parsing data file'
	exit(1)

#write html 
outputfile.write('<body style="margin:0;">\n')

index_id = file_len(datafile)

#write image html
for i in range(0, len(full_items), 3) :
	outputfile.write('<a href = http://www.' + full_items[i+1].strip('\n') + ' >\n')
	outputfile.write('<img src = http://www.'+ full_items[i].strip('\n') + ' style="width:100%; height: auto" >')
	outputfile.write(''+full_items[i+2].strip('\n'))
	outputfile.write('</a>\n')
	datafile.write(full_items[i].strip('\n')+ ' ' + full_items[i+1].strip('\n') + ' ' + str(parseint(full_items[i+2])) + ' 0 '+ str(index_id) + '\n')
	index_id = index_id+1

outputfile.write('</body>')
inputfile.close()
outputfile.close()
datafile.close()

