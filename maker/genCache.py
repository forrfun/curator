import sys
from mariaDB import mariaDBops

if len(sys.argv) is 1:
	print "need arg (size of cache data)"
	exit(0);

file_name = "urlCache"

cache_file = open(file_name, 'w')

cache_size = sys.argv[1]
#cache_size = "0"

DB = mariaDBops()

for item in DB.get_items(cache_size):
    """
    print item[0] #id
    print item[1] #imgurl
    print item[2] #linkurl
    print item[3] #price
    print item[4] #status
    print "========"
    """
    cache_id =  str(item[0])
    image_url =  item[1]
    link_url =  item[2]
    price =  str(item[3])
    status =  str(item[4])
    cache_file.write(image_url+' '+link_url+' '+price+' '+status+' '+cache_id+'\n')
cache_file.close() 

