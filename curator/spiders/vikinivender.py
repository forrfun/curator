# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "vikinivender"
    allowed_domains = ["vikinivender.com"]
    start_urls = (
        'http://www.vikinivender.com/shop/shopbrand.html?xcode=038&mcode=&type=Y&scode=&sort=price2',
    )

    def parse(self, response):
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []

        resp = response.xpath('//div[contains(@class,"item-page")]')
        # get url_list
        for href in resp.xpath('a/@href'):
            #print "jwjw-href=" + href.extract()
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            #print "jwjw-"+url
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        lists = sel.xpath('//dl[contains(@class,"item-list")]')
        for item in lists:
            #print item
            imageURL = item.xpath('dt/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])

            linkURL = item.xpath('@onclick').extract()
            linkURL = linkURL[0].replace('location.href=','')
            linkURL = linkURL.replace('\'','')
            #print linkURL 
            #print price[0]
            price = item.xpath('//li[contains(@class,"prd-price")]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            #print price

            #print imagePath
            storage.store_site_url_with_price('vikinivender.com', linkURL, imageURL, price)
