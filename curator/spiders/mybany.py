# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "mybany"
    allowed_domains = ["mybany.com"]
    start_urls = (
        'http://mybany.com/product/list.html?cate_no=122',
    )
    exception_list = [
        '',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        if os.path.exists(self.cascPath) == False:
            self.logger.info('%s file is needed.', self.cascPath)
            raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []
        # get url_list
        for href in response.xpath('//div[contains(@class,"common_paging")]/ol/li/a/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            url_list.append(url)
        print url_list
        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        # use faceCascade for face detection
        face = FaceDetection()
        lists = sel.xpath('//div[contains(@class,"box")]')
        for item in lists:
            imageURL = item.xpath('a/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('div/p[contains(@class,"price")]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            imageURL[0] = imageURL[0].replace('http://mybany.com','')
            storage.store_site_url_with_price('mybany.com',item.xpath('a/@href').extract(), imageURL, price)
