# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "thedalkom"
    allowed_domains = ["thedalkom.co.kr"]
    start_urls = (
        'http://www.thedalkom.co.kr/shop/shopbrand.html?type=Y&xcode=006&mcode=001&sort=&page=1',
    )
    exception_list = [
        '/shopimages/ddbb7979/0120060000113.jpg',
        '/shopimages/ddbb7979/0120030000443.jpg',
        '/shopimages/ddbb7979/0120010001383.jpg',
        '/shopimages/ddbb7979/0120060000183.jpg',
        '/shopimages/ddbb7979/0120010001333.jpg',
        '/shopimages/ddbb7979/0120010001223.jpg',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        if os.path.exists(self.cascPath) == False:
            self.logger.info('%s file is needed.', self.cascPath)
            raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []
        # get url_list
        for href in response.xpath('//div[contains(@class,"item-page")]/a/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
#lists = sel.xpath('//dt[contains(@class,"thumb")]')
        lists = sel.xpath('//dl[contains(@class,"item")]')
        # use faceCascade for face detection
        face = FaceDetection()
        faceCascade = face.getFaceCascade(self.cascPath)
        for item in lists:
            imageURL = item.xpath('dt/a/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('dd/ul/li/div[contains(@class,"price")]/p[contains(@class,"price03")]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            storage.store_site_url_with_price('thedalkom.co.kr', item.xpath('dt/a/@href').extract(), imageURL, price)
