# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "havanasunday"
    allowed_domains = ["havanasunday.com"]
    start_urls = (
        'http://www.havanasunday.co.kr/product/list.html?cate_no=12',
        'http://www.havanasunday.com/product/bikini.html?cate_no=26',
        'http://www.havanasunday.com/product/bikini.html?cate_no=27',
        'http://www.havanasunday.com/product/bikini.html?cate_no=30',
        'http://www.havanasunday.com/product/bikini.html?cate_no=31',
        'http://www.havanasunday.com/product/bikini.html?cate_no=29',
        'http://www.havanasunday.com/product/bikini.html?cate_no=40',
        'http://www.havanasunday.com/product/bikini.html?cate_no=119',
    )
    exception_list = [
        'http://www.havanasunday.com/web/product/medium/b10-315.jpg',
        'http://www.havanasunday.com/web/product/medium/b16-315.jpg',
        'http://www.havanasunday.com/web/product/medium/b49-315.jpg',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        if os.path.exists(self.cascPath) == False:
            self.logger.info('%s file is needed.', self.cascPath)
            raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []
        # get url_list
        for href in response.xpath('//ol/li/a/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        lists = sel.xpath('//div[contains(@class,"xans-element- xans-product xans-product-listnormal")]/ul/li[contains(@class,"xans-record-")]')
        # use faceCascade for face detection
        face = FaceDetection()
        faceCascade = face.getFaceCascade(self.cascPath)
        for item in lists:
            imageURL = item.xpath('div[contains(@class,"box")]/a/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('div[contains(@class,"box")]/ul[contains(@class,"xans-element- xans-product xans-product-listitem")]/li/span[contains(@style,"font-size:12px;color:#000000;font-weight:bold;")]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            imageURL[0] = imageURL[0].replace('http://www.havanasunday.com','')
            storage.store_site_url_with_price('havanasunday.com',item.xpath('div[contains(@class,"box")]/a/@href').extract(), imageURL, price)
