# -*- coding: utf-8 -*-
import scrapy
import sys
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
from scrapy.exceptions import CloseSpider
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "ilovebikini"
    allowed_domains = ["ilovebikini.co.kr"]
    start_urls = (
        'http://www.ilovebikini.co.kr/shop/shopbrand.html?xcode=024&type=X', 
    )
    gif_exception_list = [
        'http://www.ilovebikini.co.kr/shopimages/lovebikini/0010070000283.gif',
        'http://www.ilovebikini.co.kr/shopimages/lovebikini/0010070000053.gif',
        'http://www.ilovebikini.co.kr/shopimages/lovebikini/0010090002813.gif',
        'http://www.ilovebikini.co.kr/shopimages/lovebikini/0010070000303.gif',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        #if os.path.exists(self.cascPath) == False:
        #    self.logger.info('%s file is needed.', self.cascPath)
        #    raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []

        # get url_list
        for href in response.xpath('//div[contains(@class,"item-page")]/a/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            url_list.append(url)

        # request page data
        print '= print url_list ='
        print url_list
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        lists = sel.xpath('//dl[contains(@class,"item-list")]')
        site_url = 'ilovebikini.co.kr'
        # use faceCascade for face detection
        #face = FaceDetection()
        #faceCascade = face.getFaceCascade(self.cascPath)
        #print lists
        for item in lists:
            # get image url
            imageURL = item.xpath('ul/dt/a/img/@src').extract()
            if imageURL == []:
                # if there is no imageURL, drop this item
                continue
            #print imageURL
            imagePath = response.urljoin(str(imageURL[0]))

            # if there is a jpg file, run a face detection method
            #if imagePath.rfind('jpg') != -1:
            #    # Detect faces in the image
            #    level = 8
            #    #faces = face.detectFace(faceCascade, imagePath, level)
            #    # if there are no faces in this image, drop this item
            #    if len(faces) < 1:
            #        continue
            #elif imagePath.rfind('gif') != -1:
            #    if imagePath in self.gif_exception_list:
            #        self.logger.info('%s is in exception list!', imagePath)
            #        continue

            price = item.xpath('ul/dd/ul/li/span[@class="price"]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            storage.store_site_url_with_price(site_url, item.xpath('ul/dt/a/@href').extract(), imageURL, price)
