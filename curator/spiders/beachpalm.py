# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "beachpalm"
    allowed_domains = ["beachpalm.co.kr"]
    start_urls = (
        'http://www.beachpalm.co.kr/front/php/category.php?cate_no=80',
    )
    exception_list = [
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_852.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_967.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_1554.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_259.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_365.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_1633.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_4200.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_3237.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_4305.jpg',
        'http://www.beachpalm.co.kr/web/product/tiny/kmk770910_2003.jpg',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        if os.path.exists(self.cascPath) == False:
            self.logger.info('%s file is needed.', self.cascPath)
            raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []

        # get url_list
        # beachpalm page cannot get [1]page url, so we need to save start_url first
        url_list.append(response.url)
        for href in response.xpath('//tr/td/a[contains(@href,"pss_item=#normal_list")]/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        lists = sel.xpath('//td[contains(@width,"25%")]')
        # use faceCascade for face detection
        face = FaceDetection()
        faceCascade = face.getFaceCascade(self.cascPath)
        for item in lists:
            imageURL = item.xpath('table/tr/td/a/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('table/tr/td/table/tr/td/font[contains(@style,"color:#FF9999;font-size:12px;font-style:;font-weight:bold")]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            if price[0].encode('utf-8').rfind('품절') != -1:
                print "품절"
                continue

            print imagePath
            imageURL[0] = imageURL[0].replace('http://www.beachpalm.co.kr','')
            storage.store_site_url_with_price('beachpalm.co.kr',item.xpath('table/tr/td/a/@href').extract(), imageURL, price)
