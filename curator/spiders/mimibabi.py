# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "mimibabi"
    allowed_domains = ["mimibabi.com"]
    start_urls = (
        'http://www.mimibabi.com/product/list.html?cate_no=48',
    )
    exception_list = [
        'http://www.mimibabi.com/web/product/tiny/201501/3811_shop1_246690.jpg',
        'http://www.mimibabi.com/web/product/tiny/201504/6483_shop1_775860.jpg',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        if os.path.exists(self.cascPath) == False:
            self.logger.info('%s file is needed.', self.cascPath)
            raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []
        # get url_list
        for href in response.xpath('//ol/li[contains(@class,"xans-record")]/a/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        lists = sel.xpath('//ul[contains(@class,"prdList column4")]/li[contains(@class,"item xans-record-")]')
        # use faceCascade for face detection
        face = FaceDetection()
        faceCascade = face.getFaceCascade(self.cascPath)
        for item in lists:
            imageURL = item.xpath('div[contains(@class,"box")]/a/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('div[contains(@class,"box")]/ul/li/span[contains(@style,"font-size:11px;color:202020;font-weight:bold;")]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            imageURL[0] = imageURL[0].replace('http://www.mimibabi.com','')
            storage.store_site_url_with_price('mimibabi.com',item.xpath('div[contains(@class,"box")]/a/@href').extract(), imageURL, price)
