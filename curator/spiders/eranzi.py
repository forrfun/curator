# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "eranzi"
    allowed_domains = ["eranzi.co.kr"]
    start_urls = (
        'http://www.eranzi.co.kr/shop/shopbrand.html?xcode=019&type=N&mcode=001',
    )
    exception_list = [
        '',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        #if os.path.exists(self.cascPath) == False:
        #    self.logger.info('%s file is needed.', self.cascPath)
        #    raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []
        # get url_list
#for href in response.xpath('//ol[contains(@class,"page")]/li/a/@href'):
        for href in response.xpath('//div[contains(@class,"item-page")]/a/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        # use faceCascade for face detection
        #face = FaceDetection()
        lists = sel.xpath('//div[contains(@class,"item-list")]/dl[contains(@class,"item")]')
        for item in lists:
            imageURL = item.xpath('dt/a/img/@src').extract()
            print imageURL
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('dd/ul/li/div[contains(@class,"price")]/span/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            storage.store_site_url_with_price('eranzi.co.kr',item.xpath('dt/a/@href').extract(), imageURL, price)
