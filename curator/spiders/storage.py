#coding: utf-8

from mariaDB import mariaDBops

class Storage:
	DB = mariaDBops()
	filename = 'storage'
	def store_link_url(self, url):
		print 'store_link_url(%s)' %url
		f = open('link_storage','w')
		f.write(url)
		f.write('\n')
		f.close()
	
	def store_img_url(self, url):
		print 'store_img_url(%s)' %url
		f = open('img_storage','w')
		f.write(url)
		f.write('\n')
		f.close()
	
	def store_url(self, link_url, img_url):
		link_url = str(link_url[0])
		img_url = str(img_url[0])
		f = open(self.filename,'a')
		f.write(img_url)
		f.write('\n')
		f.write(link_url)
		f.write('\n')
		#f.write(str(img_url))
		f.close()

	def store_site_url(self, site_url, link_url, img_url):
		link_url = str(link_url[0])
		img_url = str(img_url[0])
		f = open(self.filename,'a')
		f.write(site_url+img_url)
		f.write('\n')
		f.write(site_url+link_url)
		f.write('\n')
		#f.write(str(img_url))
		f.close()

	def store_site_url_with_price(self, site_url, link_url, img_url, price):
                if type(link_url) is list:
                    link_url = str(link_url[0])
                #print link_url
		img_url = str(img_url[0])
		
                price = str(price[0].encode('utf-8'))
		price = price.replace(',','')
		f = open(self.filename,'a')
		f.write(site_url+img_url)
		f.write('\n')
		f.write(site_url+link_url)
		f.write('\n')
		f.write(price)
		f.write('\n')
		#f.write(str(img_url))
		f.close()

	def store_item(self, site_url, link_url, img_url, price):
		link_url = str(link_url[0])
		img_url = str(img_url[0])
		price = price[0].replace(',','')
		price = price.encode('utf-8')
		self.DB.add_item(site_url+img_url, site_url+link_url, price)
