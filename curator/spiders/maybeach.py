# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "maybeach"
    allowed_domains = ["maybeach.co.kr"]
    start_urls = (
        'http://www.maybeach.co.kr/product/list.html?cate_no=12',
    )
    exception_list = [
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1373.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_810.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1374.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_808.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_174.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1430.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1066.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1341.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1482.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_709.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1190.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_548.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_547.jpg',
        'http://www.maybeach.co.kr/web/product/tiny/maybeach_1592.jpg',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        if os.path.exists(self.cascPath) == False:
            self.logger.info('%s file is needed.', self.cascPath)
            raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []

        # get url_list
        for href in response.xpath('//ol/li/a/@href'):
            url = response.urljoin(href.extract())
            if url in url_list:
                continue
            print "jwjw-"+url
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        lists = sel.xpath('//div[contains(@class,"box")]')
        # use faceCascade for face detection
        #face = FaceDetection()
        #faceCascade = face.getFaceCascade(self.cascPath)
        for item in lists:
            imageURL = item.xpath('div/a/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('ul/li/span/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            imageURL[0] = imageURL[0].replace('http://www.maybeach.co.kr','')
            storage.store_site_url_with_price('maybeach.co.kr',item.xpath('div/a/@href').extract(), imageURL, price)
