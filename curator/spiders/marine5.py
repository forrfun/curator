# -*- coding: utf-8 -*-
import scrapy
import os.path

from storage import Storage
from scrapy.selector import Selector
from scrapy.http import Request
#from faceDetection import FaceDetection

class BikiniSpider(scrapy.Spider):
    name = "marine5"
    allowed_domains = ["marine5.co.kr"]
    start_urls = (
        'http://www.marine5.co.kr/shop/shopbrand.html?xcode=136&type=O',
    )
    exception_list = [
        '/shopimages/coolnsweet/1360000004443.jpg?1341472174',
    ]
    cascPath = 'haarcascade_frontalface_default.xml'

    def parse(self, response):
        # check cascade file existence
        if os.path.exists(self.cascPath) == False:
            self.logger.info('%s file is needed.', self.cascPath)
            raise CloseSpider('need a cascade file.')
        self.logger.info('parse %s page for getting all pages URL!', response.url)
        url_list = []
        # get url_list
        # marine5 page cannot get [1]page url, so we need to save start_url first
        url_list.append(response.url)
        for href in response.xpath('//span[contains(@id,"mk_pager")]/a/@href'):
            url = response.urljoin(href.extract())
            print url
            if url in url_list:
                continue
            url_list.append(url)

        # request page data
        for page in url_list:
            yield scrapy.Request(page, callback=self.parse_page)

    def parse_page(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        sel = Selector(response)
        storage = Storage()
        storage.filename = self.name
        # use faceCascade for face detection
        face = FaceDetection()
        lists = sel.xpath('//table[contains(@class,"product_table")]')
        for item in lists:
            imageURL = item.xpath('tr/td[contains(@class,"Brand_prodtHeight")]/a/img/@src').extract()
            if imageURL == []:
                continue

            imagePath = str(imageURL[0])
            if imagePath in self.exception_list:
                continue

            price = item.xpath('tr/td[contains(@class,"brandprice_tr")]/span/span[contains(@class,"mk_price")]/text()').extract()
            if price == []:
                print "price data is empty, so we drop this item"
                continue

            print imagePath
            storage.store_site_url_with_price('marine5.co.kr',item.xpath('tr/td[contains(@class,"Brand_prodtHeight")]/a/@href').extract(), imageURL, price)
