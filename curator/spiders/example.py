# -*- coding: utf-8 -*-
import scrapy


class ExampleSpider(scrapy.Spider):
    name = "example"
    allowed_domains = ["example.com"]
    start_urls = (
        'http://www.example.com',
    )

    def parse(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
	print response.body
	pass
