"""bikini_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from bikini_django_app.bikini_handler import bikini_v1_items
from bikini_django_app.bikini_handler import bikini_v2_items
from bikini_django_app.bikini_handler import bikini_v1_stat
from bikini_django_app.bikini_handler import bikini_v1_plus
from bikini_django_app.bikini_handler import index
from bikini_django_app import views

#for version 2
bikini_plus = views.BikiniDataViewSet.as_view({'post':'plus'})

#for version 3
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'item', views.BikiniDataViewSet) #can make url like e.g ( http://URL/item/{argument}/{function name} )

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^bikini/v1/items/', bikini_v1_items),
    url(r'^bikini/v2/items/', bikini_v2_items),
    url(r'^bikini/v1/stat/', bikini_v1_stat),

    #version 1 : occur csrf
    url(r'^bikini/v1/(?P<id>[0-9]+)/plus/', bikini_v1_plus),
    #version 2 : not good but it's work
    #url(r'^bikini/v2/plus/$', views.BikiniDataViewSet.as_view({'post':'plus'}), name='bikini-plus' ),
    #version 3 : recommanded
    url(r'^bikini/v3/', include(router.urls)),

    url(r'^auth-api/', include('rest_framework.urls', namespace='rest_framework')),
]

#urlpatterns = format_suffix_patterns(urlpatterns)
