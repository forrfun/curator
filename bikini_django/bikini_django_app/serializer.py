from django.contrib.auth.models import User
from bikini_django_app.models import BikiniData
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class BikiniDataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BikiniData
        fields = ('imgurl', 'linkurl', 'price', 'plus', 'minus', 'block', 'utime')
