import json
import os
import logging

from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from bikini_django_app.models import BikiniData

logger = logging.getLogger(__name__)

def index(request):
    return HttpResponse("Hi, This is bikini project server!");

def bikini_v1_stat(request):
    return HttpResponse("Hi, This is bikini project server!");

def bikini_v1_plus(request):
    return HttpResponse("Hi, This is bikini project server!");

def bikini_v1_items(request):
    errMsg = ''
    result = '' # success | fail
    response_data = {}
    item_data = []
    req_item_id = 0

    # For Debugging
    #request_dump(request)

    req_item_id = request.GET.__getitem__('id')
    logger.debug("req_item_id=%s" % req_item_id)

    item_data = get_item_data(req_item_id)
    response_data['data'] = item_data

    if len(item_data) > 0:
        errMsg = ''
        result = 'success'
    else:
        errMsg = 'No available item'
        result = 'fail'

    response_data['errMsg'] = errMsg
    response_data['result'] = result

    response = HttpResponse(json.dumps(response_data), content_type="application/json");

    return response

def bikini_v2_items(request):
    errMsg = ''
    result = '' # success | fail
    response_data = {}
    req_item_id = 0

    # For Debugging
    #request_dump(request)

    req_item_id = request.GET.__getitem__('id')
    limit = request.GET.__getitem__('limit')
    logger.debug("req_item_id=%s" % req_item_id)
    logger.debug("limit=%s" % limit)

    response_data['data'] = get_item_data_from_db(req_item_id, limit)

    if len(response_data['data']) > 0:
        errMsg = ''
        result = 'success'
    else:
        errMsg = 'No available item'
        result = 'fail'

    response_data['errMsg'] = errMsg
    response_data['result'] = result

    response = HttpResponse(json.dumps(response_data), content_type="application/json");

    return response

def get_item_data_from_db(req_item_id, limit):
    item_data = []

    for item in BikiniData.objects.filter(id__gte=req_item_id)[:int(limit)]:
        data = {}
        data['id'] = item.id
        data['imageurl'] = item.imgurl
        data['linkurl'] = item.linkurl
        data['price'] = item.price
        data['status'] = item.block
        item_data.append(data)

    return item_data

def get_item_data(req_item_id):
    item_data = []
    idx = 0

    url_file_path = os.path.dirname(os.path.realpath(__file__)) #this file location (bikini_django_app)
    url_file_path = url_file_path + "/urldata"
    #logger.debug(url_file_path)
    f = open(url_file_path, 'r')

    while True:
        line = f.readline()
        idx += 1
        if not line: break
        if idx >= int(req_item_id):
            urls = line.split(' ')
            data = {}
            data['id'] = idx
            data['imageurl'] = urls[0]
            data['linkurl'] = urls[1].split('\n')[0]
            item_data.append(data)

    return item_data


def request_dump(request):
    print_member_string('request.scheme', request.scheme)
    print_member_string('request.body', request.body)
    print_member_string('request.path', request.path)
    print_member_string('request.path_info', request.path_info)
    print_member_string('request.method', request.method)
    print_member_string('request.META', request.META)
    print_member_string('request.META(\'HTTP_ACCESS-CONTROL-REQEUST-METHOD\')', request.META.get('HTTP_ACCESS-CONTROL-REQEUST-METHOD'))

def print_member_string(member, value):
    value2 = 'null'
    if value:
        value2 = value
    logger.debug("%s = %s" % (member, value2))

