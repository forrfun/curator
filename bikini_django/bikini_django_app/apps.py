from __future__ import unicode_literals

from django.apps import AppConfig


class BikiniDjangoAppConfig(AppConfig):
    name = 'bikini_django_app'
