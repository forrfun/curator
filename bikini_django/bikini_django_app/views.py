from django.shortcuts import render
# Create your views here.
import logging
from django.contrib.auth.models import User, Group
from django.db.models import F
from bikini_django_app.models import BikiniData
from bikini_django_app.serializer import UserSerializer, BikiniDataSerializer

#from rest_framework import viewsets
from rest_framework import generics, permissions, renderers, viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route

#class UserViewSet(viewsets.ReadOnlyModelViewSet):
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class BikiniDataViewSet(viewsets.ModelViewSet):
    queryset = BikiniData.objects.all()
    serializer_class = BikiniDataSerializer
#    logger = 0
#
#    def __init__(self):
#        self.logger = logging.getLogger(__name__+'.'+type(self).__name__)

    @detail_route(methods=['post'])
    def plus(self, request, pk=None, format=None):
#        self.logger.debug("plus req_item_id = " + pk)
        result = BikiniData.objects.filter(id=pk).update(plus=F('plus') + 1)
        return Response(result)

    @detail_route(methods=['post'])
    def minus(self, request, pk=None, format=None):
#        self.logger.debug("minus req_item_id = " + pk)
        result = BikiniData.objects.filter(id=pk).update(minus=F('minus') + 1)
        return Response(result)
