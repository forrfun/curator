from __future__ import unicode_literals

from django.db import models

# Create your models here.

class BikiniData(models.Model):
    imgurl = models.TextField(max_length=512)
    linkurl = models.TextField(max_length=512)
    price = models.IntegerField(default=0)
    plus = models.IntegerField(default=0)
    minus = models.IntegerField(default=0)
    block = models.IntegerField(default=0)
    utime = models.DateTimeField(auto_now=True)
#    imgurl = models.CharField(max_length=1000)
#    linkurl = models.CharField(max_length=1000)
#    price = models.IntegerField()
#    plus = models.IntegerField()
#    minus = models.IntegerField()
#    block = models.IntegerField()
#    utime = models.DateTimeField(auto_now=True)

#    class Meta:
#        db_table = 'item_test'
#        managed = False

